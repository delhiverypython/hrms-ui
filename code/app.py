from flask import Flask
from routes.reimbursement.reimbursement_route import reimburse
from routes.auth.auth import auth
from routes.onboarding.employee_onboarding import emp
from routes.performance_review.performance_review import bp
app = Flask(__name__)
app.secret_key = 'secret_key'

app.register_blueprint(reimburse)
app.register_blueprint(auth)
app.register_blueprint(emp)
app.register_blueprint(bp)

@app.route('/')
def index():
    return "hello world"


if __name__ == "__main__":
    app.run(debug=True,port=5001)