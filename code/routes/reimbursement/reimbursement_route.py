from flask import Flask, Blueprint, render_template, request, redirect, flash, url_for
import urllib.request
import json


reimburse = Blueprint('reimburse',__name__)


# To load all reimbursement request on page
@reimburse.route('/<int:id>/reimburse_info')
def get_reimburse(id):
    url = "http://127.0.0.1:5000/"+str(id)+"/reimbursement"
    resp = urllib.request.urlopen(url)
    resp = resp.read().decode('utf-8')
    resp = json.loads(resp)
    return render_template('reimbursement.html',resp = resp, id=id)

# form to request new reimbursement
@reimburse.route('/<int:id>/reimburse_form')
def form(id):
    return render_template('reimbursement_form.html',id=id)

# After successful request sending processing part is here
@reimburse.route('/<int:id>/reimbursement/processing',methods=['GET','POST'])
def process_request(id):
    if request.method == "POST":
        url = "http://127.0.0.1:5000/"+str(id)+"/reimbursement"
        headers = {}
        headers['Content-Type'] = 'application/json'
        data = {
            "id" : id,
            "super_id" : 200,
            "title" : request.form['title'],
            "reim_type" : request.form['type'],
            "date" : request.form['date'],
            "amount" : request.form['amount'],
            "comment" : request.form['comment']
            }

        req = urllib.request.Request(url,headers=headers,data=bytes(json.dumps(data), encoding="utf-8"))
        try:
            resp = urllib.request.urlopen(req)
            resp = resp.read().decode('utf-8')
            resp = json.loads(resp)
            try:
                if resp['message']:
                    flash(resp,'success')
            except:
                flash(resp,'error')    
        except urllib.error.HTTPError as e:
            resp = e.read().decode('utf-8')

        return redirect(url_for('reimburse.form',id = id))

# Here the delete button ui will delete the request
@reimburse.route('/<int:id>/reimbursement/<int:r_id>',methods=['GET'])
def remove_request(id,r_id):
    url = "http://127.0.0.1:5000/"+str(id)+"/reimbursement"
    headers = {}
    headers['Content-Type'] = 'application/json'
    data = {
        "r_id" : r_id
        }
    req = urllib.request.Request(url,headers=headers,data=bytes(json.dumps(data), encoding="utf-8"),method='DELETE')
    try:
        resp = urllib.request.urlopen(req)
        resp = resp.read().decode('utf-8')
        resp = json.loads(resp)
        try:
            if resp['message']:
                flash(resp,'success')
        except:
            flash(resp,'error')    
    except urllib.error.HTTPError as e:
        resp = e.read().decode('utf-8')

    return redirect(url_for('reimburse.get_reimburse',id = id))


# filtering part in reimbursement page
@reimburse.route('/<int:id>/reimbursement/search',methods=['GET','POST'])
def custom_filter(id):
    if request.method == "POST":
        if request.form['reim_type'] == 'All':
            return redirect(url_for('reimburse.get_reimburse',id = id))
        url = "http://127.0.0.1:5000/"+str(id)+"/reimbursement/?"
        params = {
            "reim_type" : request.form['reim_type']
        }
        params = urllib.parse.urlencode(params)
        url = url + params
        resp = urllib.request.urlopen(url)
        resp = resp.read().decode('utf-8')
        resp = json.loads(resp)
        return render_template('reimbursement.html',resp = resp, id=id)

#Here the all pending request actions can be found
@reimburse.route('/<int:id>/reimbursement/action')
def request_action(id):
    url = "http://127.0.0.1:5000/"+str(id)+"/reimbursement/action"
    resp = urllib.request.urlopen(url)
    resp = resp.read().decode('utf-8')
    resp = json.loads(resp)
    return render_template('reimbursement_action.html',id = id, resp = resp)

# filtering part in action page
@reimburse.route('/<int:id>/reimbursement/action/search',methods=['GET','POST'])
def status_filter(id):
    if request.method == "POST":
        url = "http://127.0.0.1:5000/"+str(id)+"/reimbursement/action/?"
        params = {
            "status" : request.form['status']
        }
        params = urllib.parse.urlencode(params)
        url = url + params
        resp = urllib.request.urlopen(url)
        resp = resp.read().decode('utf-8')
        resp = json.loads(resp)
        return render_template('reimbursement_action.html',resp = resp, id=id)

# manager function to accept or reject the reimbursement request
@reimburse.route('/<int:id>/reimbursement/action/<int:r_id>/<string:status>')
def request_status(id,r_id,status):
    url = "http://127.0.0.1:5000/"+str(id)+"/reimbursement/action"
    data = {
        "r_id": r_id,
        "status":status
    }
    headers = {}
    headers['Content-Type'] = 'application/json'
    req = urllib.request.Request(url,headers=headers,data=bytes(json.dumps(data), encoding="utf-8"),method='PUT')
    try:
        resp = urllib.request.urlopen(req)
        resp = resp.read().decode('utf-8')
        resp = json.loads(resp)
        try:
            if resp['message']:
                flash(resp,'success')
        except:
            flash(resp,'error')    
    except urllib.error.HTTPError as e:
        resp = e.read().decode('utf-8')

    return redirect(url_for('reimburse.request_action',id = id))
