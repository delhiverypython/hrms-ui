from flask import Blueprint,render_template,url_for,session,redirect,request,flash
import requests
bp = Blueprint('performance',__name__,url_prefix='/performance')

@bp.route('/request_feedback', methods=['GET','POST'])
def request_feedback():
    url = f"http://localhost:5000/employees/{session['email']}"
    response = requests.get(url).json()

    if request.method == "POST":
        requesteeId = request.form['requesteeId']
        context = request.form['context']

        url = "http://localhost:5003/requestFeedback/request"
        response = requests.post(url,json={
            'requestee':requesteeId,
            'context': context
        },headers={ "Authorization":f"JWT {session['token']}"})
        if response.status_code == 200:
            flash("Request sent successfully","success")
            return redirect(url_for('performance.request_feedback',response=response))
        else:
            flash(response.text,"danger")
            return redirect(url_for('performance.request_feedback',response=response))

    return render_template('feedback/request_feedback.html',response=response)

@bp.route('/give_feedback', methods=['GET','POST'])
@bp.route('/give_feedback/<int:rid>', methods=['GET','POST'])
@bp.route('/give_feedback/<int:rid>/<int:reciever>/<string:context>', methods=['GET','POST'])
def give_feedback(reciever=None,context=None,rid=None):
    url = f"http://localhost:5000/employees/{session['email']}"
    response = requests.get(url).json()
    print(reciever)
    print("*****")
    print(context)
    if request.method == "POST":
        reciever = request.form['reciever']
        context = request.form['context']
        feedback  = request.form['feedback']
        url = "http://localhost:5003/giveFeedback/give_feedback"
        response = requests.post(url,json={
            'reciever':reciever,
            'context': context,
            'feedback': feedback,
            'requestId': rid
        },headers={ "Authorization":f"JWT {session['token']}"})
        if response.status_code == 200:
            flash("Request sent successfully","success")
            return redirect(url_for('performance.give_feedback',response=response))
        else:
            flash(response.text,"danger")
            return redirect(url_for('performance.request_feedback',response=response))
    return render_template('feedback/give_feedback.html',response=response,reciever=reciever,context=context,rid=rid)

@bp.route("/given", methods=['GET','POST'])
def given():
    if request.method == "POST":
        eid = request.form['eid']
        if eid is not None:
            url= f"http://localhost:5003/giveFeedback/given/{eid}"
    if request.method == "GET":
        url="http://localhost:5003/giveFeedback/given"
    response = requests.get(url,
                headers={ "Authorization":f"JWT {session['token']}"})
    return render_template('feedback/given_feedback.html',given=response.json())

@bp.route("/recieved", methods=['GET','POST'])
def recieved():
    if request.method == "POST":
        eid = request.form['eid']
        if eid is not None:
            url= f"http://localhost:5003/giveFeedback/recieved/{eid}"

    if request.method == "GET":    
        url="http://localhost:5003/giveFeedback/recieved"
    response = requests.get(url,
                headers={ "Authorization":f"JWT {session['token']}"})
    return render_template('feedback/recieved_feedback.html',recieved=response.json())

@bp.route("/requested_by_you", methods=['GET','POST'])
def requested_by_you():
    if request.method == "POST":
        status = request.form['status']
        if status is not None:
            url= f"http://localhost:5003/requestFeedback/requested_by_you/{status}"
    if request.method == "GET":
        url="http://localhost:5003/requestFeedback/requested_by_you"
    response = requests.get(url,
                headers={ "Authorization":f"JWT {session['token']}"})
    return render_template('feedback/requested_by_you.html',requested=response.json())

@bp.route("/requested_from_you", methods=['GET','POST'])
def requested_from_you():
    if request.method == "POST":
        status = request.form['status']
        if status is not None:
            url= f"http://localhost:5003/requestFeedback/requested_from_you/{status}"
    if request.method == "GET":
        url="http://localhost:5003/requestFeedback/requested_from_you"
    response = requests.get(url,
                headers={ "Authorization":f"JWT {session['token']}"})
    return render_template("feedback/requested_from_you.html",requested=response.json())