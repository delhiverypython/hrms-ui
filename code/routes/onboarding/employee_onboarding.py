from flask import Blueprint,flash,request,session,redirect,url_for,render_template
import requests

emp = Blueprint('employee_onboarding',__name__)

@emp.route('/onboarding',methods=['GET','POST'])
def onboarding():
    response = requests.get(f"http://localhost:5000/employees/{session['email']}").json()
    if "dev" in response['designation'] and "developer" in response['designation']:
        return render_template('noaccess.html')
    if request.method == "POST":
        f = request.files['file']
        f.save(f.filename)
        # print(session['token'])
        try:
            url = "http://localhost:5000/csv/onboarding"
            files= {'csv': open(f.filename,'rb')}
            
            response = requests.post(url, files=files,
                headers={ "Authorization":f"JWT {session['token']}"})
            if response.status_code == 200:
                flash("Values inserted successfully","success")
            else:
                flash(response.text,"danger")
        except Exception as e:
            print(e)
            flash(str(e),"danger")
            return redirect(url_for('employee_onboarding.onboarding'))
        # print("response status code",response.status_code)
    return render_template('onboarding.html')