from flask import Blueprint,flash,request,session,redirect,url_for,render_template
import requests

auth = Blueprint('auth',__name__)

@auth.route('/',methods=['GET','POST'])
@auth.route("/login",methods=['GET','POST'])
def login():
    if request.method == "POST":
        email = request.form['email']
        password = request.form['password']

        response = requests.post("http://localhost:5007/auth",json={
            "username":email,
            "password": password
        })
        response_json = response.json()
        # print(type(response.status_code))
        if response.status_code == 200:
            print(response_json["access_token"])
            session['token'] = response_json["access_token"]
            session['email'] = email
            return redirect(url_for("auth.home"))
        else:
            flash("Login failed check email and password","danger")
            return redirect(url_for('auth.login'))
    return render_template('login.html')

@auth.route('/logout')
def logout():
    session.clear()
    return redirect(url_for('auth.login'))

@auth.route('/home')
def home():
    response = requests.get(f"http://localhost:5000/employees/{session['email']}").json()
    print(response['designation'])
    return render_template('home.html',designation=response['designation'])